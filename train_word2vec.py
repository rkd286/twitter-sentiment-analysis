import numpy as np
import pandas as pd

from gensim.models import Word2Vec
from gensim.utils import simple_preprocess

from data_preprocessing import get_data

def main():
    X_train, _, X_test, _ = get_data()

    df = pd.concat([X_train, X_test])
    tokens = list(tokenizer(df.values))

    model = Word2Vec(sentences=tokens, min_count=1, window=8, size=500, sg=1)

    model.wv.save_word2vec_format('input/Custom/twitter.bin', binary=True)

def tokenizer(content_array):
    for sentence in content_array:
        yield(simple_preprocess(str(sentence), deacc=True))

if __name__ == "__main__":
    main()