# Sentiment Analysis on Twitter Data

## Dataset
https://datahack.analyticsvidhya.com/contest/linguipedia-codefest-natural-language-processing-1/

## Approach
Gensim's simple_preprocess method was used to tokenize the data.


**Approach 1**<br/>
Applied GRU(and LSTM) with randomly initialized Embedding, Word2Vec(GoogleNews-300d), Glove and Custom Word2Vec.

**Approach 2**<br/>
Applied Random Forest Classifier. Vectorized using Word Embeddings

**Approach 3**<br/>
Bag of Words approach. Used a TfIdf Vectorizer followed by a MultinomialNB Classifier.<br/><br/>

**The Bag of words approach gave the best results!**
