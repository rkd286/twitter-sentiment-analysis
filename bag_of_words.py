import numpy as np
import pandas as pd
from gensim.utils import simple_preprocess
import spacy

from data_preprocessing import get_data

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import MultinomialNB, ComplementNB
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
import re

class PreprocessData:

    def __init__(self, docs):
        self.docs = docs
        self.tokens = []
        content_array = self.docs.values
        print("Tokenizing the documents...")
        for sentence in content_array:
            self.tokens.append(simple_preprocess(sentence, deacc=False))
        print("Done.")
    

    def lemmatizer(self, allowed_postags = ['NOUN', 'VERB', 'ADJ', 'ADV']):
        
        self.lemmatized = []
        nlp = spacy.load('en', disable = ['ner', 'parser'])
        print('Lemmatizing...')
        for text in self.tokens:
            document = nlp(" ".join(text))
            self.lemmatized.append(" ".join([token.lemma_ if token.lemma_ not in ['-PRON-'] else '' for token in document 
                if token.pos_ in allowed_postags]))
        print('Done.')
        return self.lemmatized

def replace_url(string):
    try:
        return string.replace(re.search("(?P<url>https?://[^\s]+)", string).group("url"), '')
    except:
        return string
    

def main():
    X, y, X_test, test_ids = get_data()
    test_len = len(X_test)
    
    tweets = pd.concat([X, X_test])
    tweets = tweets.apply(replace_url)
    preprocess = PreprocessData(tweets)
    sentences = []
    for text in preprocess.tokens:
        sentences.append(" ".join(text))
    # lemmatized = preprocess.lemmatizer()

    vectorizer = TfidfVectorizer(
    stop_words='english',
    lowercase=False,
    min_df=5,
    token_pattern='[a-zA-Z0-9]{2,}',
    ngram_range=(1,2)
    )

    vectorized_data = vectorizer.fit_transform(sentences)
    X_train = vectorized_data[:-test_len]
    X_test = vectorized_data[-test_len:]
    
    # X_dev, X_val, y_dev, y_val = train_test_split(vectorized_data, y, test_size=0.30, stratify=y)

    # model = RandomForestClassifier(n_estimators=256,
    #                                 criterion='gini',
    #                                 max_depth=55
    #                                 )
    # model.fit(X_dev, y_dev)
    # preds = model.predict(X_val)
    # print(accuracy_score(y_val, preds))
    
    model = MultinomialNB(alpha=0.50)
    model.fit(X_train, y)
    preds = model.predict(X_test)

    df_submission = pd.DataFrame({'id':test_ids, 'label':preds})
    df_submission.to_csv('output/submission_bog.csv', index=False)

if __name__ == "__main__":
    main()
