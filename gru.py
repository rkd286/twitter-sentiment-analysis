from warnings import filterwarnings
filterwarnings('ignore')

import pandas as pd

from tensorflow.train import AdamOptimizer
from tensorflow.python.keras import Sequential
from tensorflow.python.keras.layers import Dense, Embedding, GRU, LSTM
from tensorflow.python.keras.initializers import Constant
from tensorflow.python.keras import backend as K

from data_preprocessing import preprocess

from sklearn.utils import shuffle

def main():

    X_train, y_train, X_test, test_ids, We, n_features, vocab_size, sequence_length = preprocess()

    epochs = 10
    batch_size = 32
    hidden_layer_size = 32
    learning_rate = 1e-2

    model = Sequential()
    embedding_layer = Embedding(vocab_size, 
                                n_features, 
                                embeddings_initializer=Constant(We), 
                                input_length=sequence_length, 
                                trainable=True)
    
    model.add(embedding_layer)
    model.add(GRU(units=hidden_layer_size, dropout=0.5, recurrent_dropout=0.5))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(loss='binary_crossentropy',
                 optimizer=AdamOptimizer(learning_rate=learning_rate), 
                 metrics=['accuracy', f1])

    X_train, y_train = shuffle(X_train, y_train)
    model.fit(X_train, y_train, batch_size=batch_size, epochs=epochs)
    preds = model.predict(X_test)
    preds = [1 if val[0] > 0.5 else 0 for val in preds]
    df_submission = pd.DataFrame({'id':test_ids, 'label':preds})
    df_submission.to_csv('output/submission_gru.csv', index=False)


def f1(y_true, y_pred):
    def recall(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision
    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))

if __name__ == "__main__":
    main()