from warnings import filterwarnings
filterwarnings('ignore')

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import re
from vectorizer import Word2VecVectorizer, GloveVectorizer, CustomVectorizer

from sklearn.metrics import f1_score
from sklearn.model_selection import train_test_split

from tensorflow.python.keras.preprocessing.text import Tokenizer
from tensorflow.python.keras.preprocessing.sequence import pad_sequences

def get_data(path_train='input/train_2kmZucJ.csv', path_test='input/test_oJQbWVk.csv'):

    df = pd.read_csv(path_train, sep=',')
    X_train = df['tweet']
    y_train = df['label'].values

    df_test = pd.read_csv(path_test)
    test_ids = df_test['id']
    X_test = df_test['tweet']

    return X_train, y_train, X_test, test_ids

def preprocess():

    n_features=500
    # test_size = 0.20
    X_train, y_train, X_test, test_ids = get_data()
    
    
    tweets = pd.concat([X_train, X_test])
    def replace_url(string):
        try:
            string = string.replace(re.search("(?P<url>https?://[^\s]+)", string).group("url"), '')
        except:
            pass
        return string 

    tweets = tweets.apply(replace_url)
    # X_train, X_test, y_train, y_test = train_test_split(tweets, y, test_size=0.20, stratify=y)

    tokens = Tokenizer()
    tokens.fit_on_texts(tweets.values)

    #pad sequences
    word_index = tokens.word_index
    sequence_length = 24
    # sequence_length = max([len(s.split()) for s in tweets])

    X_tokens_train = tokens.texts_to_sequences(X_train)
    X_tokens_test = tokens.texts_to_sequences(X_test)
    
    
    X_pad_train = pad_sequences(X_tokens_train, maxlen=sequence_length, padding='post')
    X_pad_test = pad_sequences(X_tokens_test, maxlen=sequence_length, padding='post')

    #Embedding Matrix
    vocab_size = len(word_index) + 1
    embeddings = np.random.randn(vocab_size, n_features)
    vectorizer = CustomVectorizer()
    count_words = 0
    for word, index in word_index.items():
        if index > vocab_size:
            continue
        try:
            embedding_vector = vectorizer.model.get_vector(word)
            embeddings[index] = embedding_vector
        except KeyError:
            count_words+=1
    
    print("No. of words not in the trained word vector model: {}".format(count_words))
    
    return X_pad_train, y_train, X_pad_test, test_ids, embeddings, n_features, vocab_size, sequence_length



