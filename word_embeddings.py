import pandas as pd

from data_preprocessing import get_data
from vectorizer import Word2VecVectorizer, CustomVectorizer

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, AdaBoostClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier

def main():
    X_train, y_train, X_test, test_ids = get_data()

    vectorizer = CustomVectorizer()

    X_train_vecs = vectorizer.fit_transform(X_train)
    X_test_vecs = vectorizer.fit_transform(X_test)

    # X_dev, X_val, y_dev, y_val = train_test_split(X_train_vecs, y_train, test_size=0.30, stratify=y_train)

    # model = RandomForestClassifier(n_estimators=256,
    #                                 criterion='gini',
    #                                 max_depth=55
    #                                 )
    # model.fit(X_dev, y_dev)
    # preds = model.predict(X_val)
    # print(accuracy_score(y_val, preds))

    model = RandomForestClassifier(n_estimators=256,
                                    criterion='gini',
                                    max_depth=10
                                    )
    model.fit(X_train_vecs, y_train)
    preds = model.predict(X_test_vecs)


    df_submission = pd.DataFrame({'id':test_ids, 'label':preds})
    df_submission.to_csv('output/submission_we.csv', index=False)



if __name__ == "__main__":
    main()