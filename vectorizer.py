import numpy as np
import pandas as pd

from gensim.test.utils import datapath, get_tmpfile
from gensim.models import KeyedVectors
from gensim.scripts.glove2word2vec import glove2word2vec
from gensim.models import Word2Vec
from gensim.utils import simple_preprocess

from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier
from sklearn.metrics import accuracy_score

import os

class Vectorizer:
    def __init__(self):
        print('Parent Class')
        self.model = None
    
    def fit(self, data):
        self.n_features = self.model.get_vector('king').shape[0]
        self.n_obs = len(data)
    
    
    def transform(self, data):

        X = np.zeros((self.n_obs, self.n_features))
        empty_count = 0

        for index, sentence in enumerate(data):
            tokens = simple_preprocess(sentence, deacc=True)

            vecs = []

            for word in tokens:

                try:
                    vec = self.model.get_vector(word)
                    vecs.append(vec)
                except KeyError:
                    pass
            
            if(len(vec) > 0):
                vecs = np.array(vecs)
                X[index] = vecs.mean(axis=0)
            else:
                empty_count += 1
            
        print("Number of samples with no words found: {}/{}".format(empty_count, self.n_obs))
        return X
        
    def fit_transform(self, data):
        self.fit(data)
        return self.transform(data)

class GloveVectorizer(Vectorizer):

    def __init__(self):

        print('Loading Glove file...')
        
        project_file_path = os.path.abspath(os.curdir)
        self.glove_file = datapath(project_file_path + '/input/Glove/glove.6B.300d.txt')
        self.word2vec_glove_file = get_tmpfile('glove.6B.300d.txt')
        glove2word2vec(self.glove_file, self.word2vec_glove_file)
        self.model = KeyedVectors.load_word2vec_format(self.word2vec_glove_file)

        print('Successfully loaded Glove word vectors')

class Word2VecVectorizer(Vectorizer):

    def __init__(self):

        print('Loading Word2Vec file...')
        
        project_file_path = os.path.abspath(os.curdir)
        self.word2vec_file = project_file_path + '/input/Word2Vec/GoogleNews-vectors-negative300.bin'                             
        self.model = KeyedVectors.load_word2vec_format(self.word2vec_file, binary=True)
        
        print('Successfully loaded Word2Vec word vectors')

class CustomVectorizer(Vectorizer):

    def __init__(self):

        print('Loading Custom Word2Vec file...')
        
        project_file_path = os.path.abspath(os.curdir)
        self.word2vec_file = project_file_path + '/input/Custom/twitter.bin'                             
        self.model = KeyedVectors.load_word2vec_format(self.word2vec_file, binary=True)
        
        print('Successfully loaded Custom Word2Vec word vectors')



